//
//  NotificationDemoViewController.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/29/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit
import Blox

class NotificationDemoViewController: UIViewController {

    @IBOutlet var postButton: UIButton
    @IBOutlet var label: UILabel
    
    override var nibName: String {
        return "NotificationDemoView"
    }
    
    override func viewDidLoad() {
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        
        weak var weakSelf = self
        notificationCenter.addObserverForName("mynotification", object: nil, queue: nil){ (note: NSNotification!) in
            if var ws = weakSelf {
                var msg = note.userInfo["message"] as String
                var rectValue = note.userInfo["rect"] as NSValue
                var rect = rectValue.CGRectValue()
                println(rect)
                ws.label.text = msg
            }
        }
        
        postButton.onTouchUpInside{(sender: AnyObject) in
            var dns = NSNotificationCenter.defaultCenter()
            var rect = CGRectMake(5.0, 7.0, 100.0, 64.0)
            dns.postNotificationName("mynotification", object: nil, userInfo: ["message" : "Merry Message", "rect" : NSValue(CGRect:rect)])
        }
            
        super.viewDidLoad()
    }

    
    
    
}
