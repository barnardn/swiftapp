//
//  ViewController.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit
import Blox

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var textField : UITextField
    @IBOutlet var displayButton : UIButton
    
    override var nibName : String {
        get {
            return "MainView"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.becomeFirstResponder()
        textField.delegate = self
        navigationItem.title = "UIButton"
        
        weak var weakSelf = self

        displayButton.onTouchUpInside{ (button: AnyObject) in
            var alert : UIAlertController
            let name = weakSelf!.textField.text
            if !name.isEmpty {
                alert = UIAlertController(title: "Greetings", message: "Howdy there \(name)", preferredStyle: UIAlertControllerStyle.Alert)
                weakSelf!.displayButton.removeBlock(forControlEvent: UIControlEvents.TouchUpInside);
            } else {
                alert = UIAlertController(title: "What's Your Name?", message: "Enter your name for a custom greeting", preferredStyle: UIAlertControllerStyle.Alert)
            }            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            weakSelf!.presentViewController(alert, animated: true, completion: { println("Modal displayed")})
        }
        
    }

    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return false
    }

}

