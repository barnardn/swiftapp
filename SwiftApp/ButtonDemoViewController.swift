//
//  ButtonDemoViewController.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/25/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit
import Blox

class ButtonDemoViewController: UIViewController {

    @IBOutlet var touchUpInsideButton: UIButton
    @IBOutlet var touchUpOutsideButton: UIButton
    @IBOutlet var touchDownButton: UIButton
    @IBOutlet var removeActionsButton: UIButton
    @IBOutlet var statusLabel: UILabel
    
    override var nibName: String {
        get {
            return "ButtonDemoView"
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        touchUpInsideButton.onTouchUpInside{(sender: AnyObject) in
            self.statusLabel.text = "OnTouchUpInside"
            self.delay(1.0) { self.statusLabel.text = "" }
        }
        
        touchUpOutsideButton.onTouchUpOutside{(sender: AnyObject) in
            self.statusLabel.text = "OnTouchUpOutside"
            self.delay(1.0) { self.statusLabel.text = "" }
        }
        touchDownButton.onTouchDown{(sender: AnyObject) in
            self.statusLabel.text = "OnTouchDown"
            self.delay(1.0) { self.statusLabel.text = "" }
        }
        removeActionsButton.onTouchUpInside{(sender: AnyObject) in
            
            self.touchUpInsideButton.removeBlock(forControlEvent: UIControlEvents.TouchUpInside)
            self.touchUpOutsideButton.removeBlock(forControlEvent: UIControlEvents.TouchUpOutside)
            self.touchDownButton.removeBlock(forControlEvent: UIControlEvents.TouchDown)

        }
        
        
    }

    func delay(delay: Double, block: ()->()) {
        
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), block)
    }
    
    
    
}
