//
//  ControlHandler.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/18/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit


class ControlHandler: NSObject, NSCopying {

    var handlerBlock: ((AnyObject) -> Void)
    var controlEvents: UIControlEvents
    
    init(handler: ((AnyObject)->Void), forControlEvent controlEvent: UIControlEvents) {
        handlerBlock = handler
        controlEvents = controlEvent
    }
    
    func execute(sender: AnyObject) {
        self.handlerBlock(sender)
    }
    
    // MARK: nscopying methods
    
    func copyWithZone(zone: NSZone) -> AnyObject! {
        return ControlHandler(handler: self.handlerBlock, forControlEvent: self.controlEvents)
    }
    
}
