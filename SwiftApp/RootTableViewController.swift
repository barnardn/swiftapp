//
//  RootTableViewController.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/25/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit

let kCellId: String = "cellid"

class RootTableViewController: UITableViewController {

    var numberOfDemos = 2;

    init() {
        super.init(style: UITableViewStyle.Plain)
    }
    
    init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: kCellId)
    }
    
    // #pragma mark - Table view data source


    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return numberOfDemos
    }

    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        
        var cell = tableView.dequeueReusableCellWithIdentifier(kCellId) as UITableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        if indexPath.row == 0 {
            cell.textLabel!.text = "UIButton"
        } else {
            cell.textLabel!.text = "NSNotification"
        }

        return cell
    }
    

    // MARK - tableview delegate methods
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        switch indexPath.row {
        case 0:
            navigationController.pushViewController(ButtonDemoViewController(coder: nil), animated: true)
        case 1:
            navigationController.pushViewController(NotificationDemoViewController(coder: nil), animated: true)
        default:
            println("Hey, in default clause!")
        }
        
    }
    
}
