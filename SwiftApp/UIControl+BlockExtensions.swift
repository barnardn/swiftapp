//
//  UIControl+BlockExtensions.swift
//  SwiftApp
//
//  Created by Norm Barnard on 6/24/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

import UIKit
import Foundation

var be_objectKeySelector: Selector = "be_controlHandler"
var be_objectKey : CConstVoidPointer = &be_objectKeySelector

extension UIControl {
    
    
    func onTouchUpInside(block: (AnyObject)->Void) {
        
        self.addBlock(block, forControlEvent: UIControlEvents.TouchUpInside)
        
    }
    
    func onTouchUpOutside(block: (AnyObject)->Void) {
        
        self.addBlock(block, forControlEvent: UIControlEvents.TouchUpOutside)
        
    }
    
    func onTouchDown(block: (AnyObject)->Void) {
        
        self.addBlock(block, forControlEvent: UIControlEvents.TouchDown)
        
    }
    
    func addBlock(block: (AnyObject)->Void, forControlEvent controlEvent: UIControlEvents) {
        
        var dict: Dictionary<UInt, NSMutableSet>
        if var obj: AnyObject? = objc_getAssociatedObject(self, be_objectKey) {
            dict = obj as Dictionary<UInt, NSMutableSet>
        } else {
            dict = Dictionary<UInt, NSMutableSet>()
            dict[controlEvent.toRaw()] = NSMutableSet()
            objc_setAssociatedObject(self, be_objectKey, dict, UInt(OBJC_ASSOCIATION_RETAIN_NONATOMIC))
        }
        let controlHandler = ControlHandler(handler: block, forControlEvent: controlEvent)
        self.addTarget(controlHandler, action: "execute:", forControlEvents: controlEvent)
        if var set = dict[controlEvent.toRaw()] {
            set.addObject(controlHandler)
        }
        
    }
    
    func removeBlock(forControlEvent controlEvent: UIControlEvents) {
        
        if var dict = objc_getAssociatedObject(self, be_objectKey) as? Dictionary<UInt, NSMutableSet> {
            
            if var set = dict.removeValueForKey(controlEvent.toRaw()) {
                set.enumerateObjectsUsingBlock{(handler: AnyObject!, stop: CMutablePointer<ObjCBool>) in
                    self.removeTarget(handler, action: nil, forControlEvents: controlEvent)
                }
                if set.count == 0 {
                    dict.removeValueForKey(controlEvent.toRaw())
                }
            }
        }
    }
    
}
